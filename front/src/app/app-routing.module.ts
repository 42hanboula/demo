import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';


//import {AuthGuard} from './guards/auth-guard.service';
//import {AdminAuthGuard} from './guards/admin-auth-guard.service';

import {LoginComponent} from './login/login.component';
import {HomeComponent} from './home/home.component';
import {RatesComponent} from './rates/rates.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'rates',
    component: RatesComponent,
  //  canActivate: [AuthGuard]
  },
  {
    path: '**',
    redirectTo: '/home'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
