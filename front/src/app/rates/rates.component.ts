import { Component, OnInit } from '@angular/core';
import { RatesService } from '../service/rates.service';

@Component({
  selector: 'app-rates',
  templateUrl: './rates.component.html',
  styleUrls: ['./rates.component.css'],
  providers: [RatesService]
})
export class RatesComponent implements OnInit {

  rates: any;
  constructor(public ratesService: RatesService) { }

  ngOnInit() {
    this.getRates();
  }

  getRates() {
    this.ratesService.getRates()
    .subscribe(
      ( rates: any[]) => {
          this.rates = rates;
          console.log(rates);
        },
      (error: Response) => console.log(error)
    );
  }

}
