import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';

import { RatesComponent } from './rates/rates.component';
import { RatesService } from './service/rates.service';
import { HttpModule } from '@angular/http';

import {DataTableModule} from 'primeng/datatable';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { AppRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    RatesComponent,
    HomeComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    DataTableModule,
    AppRoutingModule
  ],
  providers: [RatesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
