import { Injectable } from '@angular/core';
import { Http, Response, Headers} from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs';

@Injectable()
export class RatesService {

  constructor(private http: Http) { }

  getRates() {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
   // headers.append('X-Requested-With', 'XMLHttpRequest');

  return this.http.get('http://localhost:9080/rates', { headers })
    .map(
      (response: Response) => {
        return response.json();
      }
    );
  }

}
