import { Component } from '@angular/core';
import {RatesComponent} from './rates/rates.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Demo angular spring boot';
}
