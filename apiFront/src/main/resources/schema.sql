/**
 @author OGUELIDA
 Model : front
**/

DROP SCHEMA IF EXISTS front;

CREATE SCHEMA front;
USE front;

/* Table: user (Application Users) */
CREATE TABLE user (
    id     NVARCHAR(20) NOT NULL,
    password    NVARCHAR(20) NOT NULL,
    firstname  NVARCHAR(50) ,
    lastname   NVARCHAR(50) ,
    CONSTRAINT id PRIMARY KEY(id)
);