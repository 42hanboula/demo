package com.apiFront.session;

import io.swagger.annotations.*;
import lombok.*;

@Data
@EqualsAndHashCode(callSuper=false)
public class SessionResponse extends OperationResponse {
  @ApiModelProperty(required = true, value = "")
  private SessionItem item;
  
}
