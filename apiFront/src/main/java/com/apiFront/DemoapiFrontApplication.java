package com.apiFront;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 
 * @author OGUELIDA
    
    SpringBoot Notes
	@Bean      :tells Spring 'here is an instance of this class, please keep hold of it and give it back to me when I ask'.
	@Autowired :says 'please give me an instance of this class, for example, one that I created with an @Bean annotation earlier'.
 */
@SpringBootApplication
public class DemoapiFrontApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoapiFrontApplication.class, args);
	}
}
