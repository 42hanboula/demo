package com.apiFront.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.apiFront.client.RestClientService;
import com.apiFront.entities.Rate;

@RestController
public class RestWebService {

	@Autowired
	private RestClientService restClientService;
	
	@RequestMapping(value="/rates", method=RequestMethod.GET)
	public List<Rate> getAllRate(){
		return restClientService.findAllRates();
	}
}
