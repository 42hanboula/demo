package com.apiFront.client;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.apiFront.entities.Rate;

@Service
public class RestClientService {

	public List<Rate> findAllRates(){
		RestTemplate restTemplate = new RestTemplate();
	      ResponseEntity<List<Rate>> ratesResponse =
	    	        restTemplate.exchange("https://bitpay.com/api/rates",
	    	                    HttpMethod.GET, null, new ParameterizedTypeReference<List<Rate>>() {
	    	            });

	    	return ratesResponse.getBody();
	}
}
