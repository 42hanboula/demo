package com.apiFront.identity;


import com.apiFront.entities.User;

public class TokenUser extends org.springframework.security.core.userdetails.User {
    private User user;

    //For returning a normal user
    public TokenUser(User user) {
        super( user.getUserId(), user.getPassword(),null);
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}
