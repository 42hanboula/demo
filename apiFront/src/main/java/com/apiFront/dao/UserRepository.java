package com.apiFront.dao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apiFront.entities.User;

public interface UserRepository  extends JpaRepository<User, String>{
	Optional<User> findOneByUserIdAndPassword(String id, String password);
	Optional<User> findOneByUserId(String userId);

}
