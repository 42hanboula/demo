# Demo

## Le projet se devise en trois parties

### apiClient
Elle reçoit les données d’une source externe('https://bitpay.com') et les enregistre dans la base de données
Elle expose ces données via un web service
URL: 'http://localhost:8080/rates'

### apiFront
Elle reçoit les données de l’api Client et les envois en front
URL: 'http://localhost:9080/rates'
Swager: 'http://localhost:9080/swagger-ui.html'

### Front
Présentation des données dans un tableau
URL: 'http://localhost:4200'



## Les technologies utilisées
Backend (REST) : Spring Boot
Persistence	JPA (avec Spring Data)
Rest
REST Documentation (Swagger UI)
Memory DB (H2)
Server Build Tools( Maven )
Frontend : (Angular 5)
Client Build Tools(angular-cli)
primeNG


### NB : partie Security : Token Based (Spring Security and JWT ) incomplete
