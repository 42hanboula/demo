package com.apiClient.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.apiClient.dao.RateRepository;
import com.apiClient.entities.Rate;

@RestController
public class RestService {
	@Autowired
	private RateRepository rateRepository;
	
	@RequestMapping(value="/rates", method=RequestMethod.GET)
	public List<Rate> getAllRate(){
		return rateRepository.findAll();
	}
}
