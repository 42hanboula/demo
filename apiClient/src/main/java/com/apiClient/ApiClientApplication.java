package com.apiClient;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.apiClient.dao.RateRepository;
import com.apiClient.entities.Rate;

@SpringBootApplication
public class ApiClientApplication implements CommandLineRunner {
	@Autowired
	private RateRepository rateRepository;
	

	public static void main(String[] args) {
		SpringApplication.run(ApiClientApplication.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		RestTemplate restTemplate = new RestTemplate();
	      ResponseEntity<List<Rate>> ratesResponse =
	    	        restTemplate.exchange("https://bitpay.com/api/rates",
	    	                    HttpMethod.GET, null, new ParameterizedTypeReference<List<Rate>>() {
	    	            });

	    	List<Rate> rates = ratesResponse.getBody();
	    	rateRepository.save(rates);
	}
}
