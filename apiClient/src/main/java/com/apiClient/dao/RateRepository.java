package com.apiClient.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.apiClient.entities.Rate;

public interface RateRepository extends JpaRepository<Rate, Long>{

}
